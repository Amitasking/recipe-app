import { NgModule } from '@angular/core';
import  { Routes, RouterModule } from '@angular/router'
import { Receipe } from './recipe/recipe.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { RecipeStartComponent } from './recipe/recipe-start/recipe-start.component';
import { RdComponent } from './recipe/rd/rd.component';
import { RecipeEditComponent } from './recipe/recipe-edit/recipe-edit.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { SpinneriiComponent } from './spinnerii/spinnerii.component';
import { Gaurd } from './auth/Gaurd.service';


const appRoute:Routes = [
  {path:'', redirectTo: '/home', pathMatch:'full'},
  { path:'home', component: HomeComponent },
  { path:'recipes', component: Receipe,  children:[{path: '', component: RecipeStartComponent,canActivate:[Gaurd]},
  {path:'new', component:RecipeEditComponent, canActivate:[Gaurd]},
  {path:':id', component: RdComponent,canActivate:[Gaurd]},
  {path:':id/edit', component: RecipeEditComponent,canActivate:[Gaurd]}
]},


  {path: 'shopping-list', component: ShoppingListComponent},
    {path:'about', component: AboutComponent},

      {path:'signup', component: SignupComponent},
      {path:'signin', component: SigninComponent},
        {path:'spin', component: SpinnerComponent},
          {path:'spin2', component: SpinneriiComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoute, {scrollPositionRestoration: 'enabled'}), ],

  exports:[RouterModule]
})
export class AppRoutingModule {}
