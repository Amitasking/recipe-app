import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { recipeService } from '../recipe.service';
import { shoppingService } from 'src/app/shopping-list/shopping.service';
import { recipeModel } from '../recipe.model';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-rd',
  templateUrl: './rd.component.html',
  styleUrls: ['./rd.component.css'],


})
export class RdComponent implements OnInit {
 recipe:recipeModel;
 id:number;

loading = new EventEmitter<any>();

loaded:boolean = false;




  constructor(private  recipeService : recipeService,private shoppingService : shoppingService, private route : ActivatedRoute, private routeTo:Router) {
    // setTimeout(function(){
    //     this.loaded = true;
    //   alert(this.loaded)},2000);
    //

   }

  ngOnInit() {

this.route.params.subscribe((id)=>{
  this.id = +id['id'];
  this.recipe= this.recipeService.getRecipeById(this.id)
})

  }

//addtoShoppingList
  onAdd(){
    this.shoppingService.addIngredients(this.recipe.ingredients )


  }
edit(){
  this.routeTo.navigate(['edit'], {relativeTo:this.route})


}



///Delete
onDelete(){
let r = confirm('Are You Sure, You Want To Delete')
  if(r){
this.recipeService.delete(this.id);
swal.fire(
  'Deleted!',
  '',
  'success'
)
this.routeTo.navigate(['../'], {relativeTo:this.route})
}
else{
  return
}

}



}
