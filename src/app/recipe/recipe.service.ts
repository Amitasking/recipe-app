import {recipeModel} from './recipe.model'
import { EventEmitter, Injectable, OnInit } from '@angular/core';
import { ingredientsModel } from '../shared/ingredients.model';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/Auth.service';
import { Router } from '@angular/router';


@Injectable()

export class recipeService implements OnInit{

e = new EventEmitter();
recipeSelected = new EventEmitter<recipeModel>();
constructor(private HttpClient:HttpClient, private AuthService:AuthService, private Router:Router){

}
recipe:recipeModel[];
 ngOnInit(){
   this.HttpClient.get('https://recipe-app-24479.firebaseio.com/recipes.json?auth=' + this.AuthService.getToken()).subscribe((d:recipeModel[])=>{console.log(d);this.recipe =d})
 }




      getRecipe(){
          return this.recipe;

      }

    getRecipeById(id:number){
      return this.recipe[id];

    }

addNewRecipe(recipe : recipeModel){






this.recipe.push(recipe)

}


updateRecipe( id, recipe ){

  this.recipe[id] = recipe;
}

delete(id){

  this.recipe.splice(id,1);
}
//get
setRecipe(recipe){
    this.Router.navigate(['/spin2'])
  recipe.subscribe((obj)=>{
      this.Router.navigate(['/recipes'])
    this.recipe = obj
    this.e.emit(obj)
  })
}

}
