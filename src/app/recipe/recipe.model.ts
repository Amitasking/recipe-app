import { ingredientsModel } from '../shared/ingredients.model';

export class recipeModel{

        name:string;
        description : string;
        image : string;
        ingredients : ingredientsModel[];


    constructor(n:string, d:string, i:string ,  ingredients : ingredientsModel[]){

        this.name  =n;
        this.description = d;
        this.image = i;
        this.ingredients = ingredients

    }


}
