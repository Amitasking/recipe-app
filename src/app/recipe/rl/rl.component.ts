import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { recipeModel } from '../recipe.model';
import { recipeService } from '../recipe.service';
import { Router, ActivatedRoute } from '@angular/router'
import { AuthService } from 'src/app/auth/Auth.service';

@Component({

templateUrl:'./rl.component.html',
selector : 'app-rl',
styleUrls: ['./rl.component.css']
})
export class rl implements OnInit{
  // @Output() e = new EventEmitter<recipeModel>();
    recipe :recipeModel[];
constructor(private recipeService:recipeService, private AuthService:AuthService,private router:Router,private actve:ActivatedRoute){  this.recipe = recipeService.getRecipe();}
   // :recipeModel[]
    // recipeWasSelected(data:recipeModel){
    //
    //   this.e.emit(data)
    //   alert(`phunch gya mai ${data}`)
    // }
    //
    ngOnInit(){this.recipeService.e.subscribe((d)=>{
     this.recipe = this.recipeService.getRecipe();
    })}

addNew(){

this.router.navigate(['new'],{relativeTo:this.actve})
}


}
