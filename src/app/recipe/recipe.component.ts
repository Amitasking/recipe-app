import { Component, OnInit } from '@angular/core';
import { recipeModel } from './recipe.model';
import { recipeService } from './recipe.service';


@Component({

templateUrl:'./recipe.component.html',
selector: 'app-recipe',
// providers : []

})
export class Receipe implements OnInit  {




recipe:recipeModel;

constructor(private recipeService:recipeService) {



}


ngOnInit() {

this.recipeService.recipeSelected.subscribe((data:recipeModel)=>{this.recipe = data});



}


}
