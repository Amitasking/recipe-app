import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { recipeService } from '../recipe.service';
import { FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import { recipeModel } from '../recipe.model';
import { ingredientsModel } from 'src/app/shared/ingredients.model';
import swal from 'sweetalert2';
@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  constructor(private route : ActivatedRoute, private recipeService: recipeService, private router: Router) { }
id:number;
editMode = false;
 form : FormGroup;
  ngOnInit() {

    this.route.params.subscribe((params)=>{
      this.id = +params['id'];
      this.editMode = params['id'] != null;
      console.log(this.editMode)
      this.initForm();
    })



  }


    initForm(){
      let recipeName = '';
      let image = '';
      let description = '';
      let recipeIngredients = new FormArray([]);

      if(this.editMode){

        const recipe = this.recipeService.getRecipeById(this.id);
        recipeName = recipe.name;
        image = recipe.image;
        description = recipe.description;
        if(recipe['ingredients']){
          for(let ingredient of recipe.ingredients){
            recipeIngredients.push(new FormGroup({
              'name' : new FormControl(ingredient.name,Validators.required ),
              'amount' : new FormControl(ingredient.amount,Validators.required ),
            }))
          }
        }

      }

          this.form = new FormGroup({

            'name' : new FormControl(recipeName, Validators.required),
            'image' : new FormControl(image,Validators.required ),
            'description' : new FormControl(description,Validators.required ),
            'ingredients' : recipeIngredients
          });


        }


///new ingre

addNew(){
(<FormArray>this.form.get('ingredients')).push(new FormGroup({
  'name' : new FormControl(null,Validators.required ),
  'amount' : new FormControl(null,Validators.required ),
}))

}


submit()
{

  const recipe = new recipeModel(this.form.value['name'],
this.form.value['description'],
this.form.value['image'],
this.form.value['ingredients']);

  if(this.editMode){

this.recipeService.updateRecipe(this.id, recipe)

swal.fire(
  'Edited!',
  '',
  'success'
)

        }

  else{
    swal.fire(
      'Recipe Added!',
      '',
      'success'
    )
  this.recipeService.addNewRecipe(recipe)




  }



this.cancel();
}
cancel(){

  this.router.navigate(['../'], {relativeTo:this.route})
}

}
