import { Component, OnInit ,Output, EventEmitter } from '@angular/core';
import {ingredientsModel} from '../../shared/ingredients.model';
import {shoppingService} from '../shopping.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
  // providers:[shoppingService]
})
export class ShoppingEditComponent implements OnInit {

constructor(private shoppingService:shoppingService){}

onAddIng(inputName,inputAmount ){




}

onAdd(form: NgForm){
const value = form.form.value;
// console.log(`form valied ${form.form.valid}`)
const ingredient = new ingredientsModel(value.name,value.amount);
this.shoppingService.addIng(ingredient);

}

delete(){
  this.shoppingService.delete()
}

clear(){
  this.shoppingService.clear()
}
  ngOnInit() {
  }

}
