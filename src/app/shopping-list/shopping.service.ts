import {ingredientsModel} from '../shared/ingredients.model';
import {EventEmitter, Output} from '@angular/core';
import swal from 'sweetalert2';

export class shoppingService{

e = new EventEmitter<ingredientsModel[]>();
clearE = new EventEmitter();
 ingredients:ingredientsModel[]  = [];

  addIng(ingredient:ingredientsModel){

      this.ingredients.push(ingredient);
    this.e.emit(this.ingredients.slice());

  }
  getIng(){

    return this.ingredients
  }

addIngredients(ingredients: ingredientsModel[]){

this.ingredients.push(...ingredients)
swal.fire(
  'ingredients Added To Cart!',
  '',
  'success'
)

  }


////
delete(){
  this.ingredients.pop()
}


clear(){

this.ingredients = null;
this.clearE.emit()
}
/////



}
