import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../Auth.service';
import { DataStorageService } from 'src/app/shared/storeData.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private AuthService:AuthService) { }

  ngOnInit() {
  }



  submit(form:NgForm)
      {
      const email = form.form.value.email;
      const Password = form.form.value.Password;
  // alert(email + Password)

  this.AuthService.signInWithEmail(email,Password)

      }




}
