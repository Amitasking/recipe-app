import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../Auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private AuthService: AuthService) { }

  ngOnInit() {
  }

submit(form:NgForm)
    {
    const email = form.form.value.email;
    const Password = form.form.value.Password;


this.AuthService.signUpWithEmail(email,Password)
    }



}
