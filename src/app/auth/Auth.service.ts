
import * as firebase from 'firebase';
import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';




@Injectable()
export class AuthService
{
e = new EventEmitter();
constructor(private Router:Router){


}


tk = null;
spin = true
  signUpWithEmail(email : string, Password: string){

      firebase.auth().createUserWithEmailAndPassword(email,Password)
      .then((d)=>{
        swal.fire(
          'Account Created!',
          '',
          'success'
        )

        this.Router.navigate(['/signin'])
      },
      (err) =>{
   this.Router.navigate(['/signup'])
          this.spin = false;
          swal.fire(
            err.message,
            '',
            'error'
          )



       }
      )


      if(this.spin){
        this.Router.navigate(['/spin'])
      }
  }


signInWithEmail(email:string,Password:string){
this.Router.navigate(['spin'])
  firebase.auth().signInWithEmailAndPassword(email,Password).then(
    (response)=>{


        firebase.auth().currentUser.getIdToken().then((token)=>{this.tk = token
this.e.emit();
      this.Router.navigate(['/recipes'])})
    },
(error)=>{
  swal.fire(
    error.message!,
    '',
    'error'
  );
     this.Router.navigate(['/signin'])
}
  )


}


getToken(){
  firebase.auth().currentUser.getIdToken().then(
    token => this.tk = token
  )

  return this.tk;
}

isAuthenticate(){
  if(this.tk == null){return false}
  else{return true }
}

  logout(){

      firebase.auth().signOut();
      this.tk = null;
        this.Router.navigate(['/home'])
        swal.fire(
          'Logged Out!',
          '',
          'success'
        )


}

}
