import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { recipeService } from '../recipe/recipe.service';
import { recipeModel } from '../recipe/recipe.model';
import { AuthService } from '../auth/Auth.service';
import { shoppingService } from '../shopping-list/shopping.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2'


@Injectable()

export class DataStorageService{

  constructor(private Router:Router ,private HttpClient: HttpClient,private shoppingService:shoppingService, private recipeService:recipeService, private AuthService:AuthService){}



    fetch(){
      const token = this.AuthService.getToken();
      this.recipeService.setRecipe(this.HttpClient.get('https://recipe-app-24479.firebaseio.com/recipes.json?auth=' + token))

    //   this.HttpClient.get('https://recipe-app-24479.firebaseio.com/recipes.json?auth=' + token).subscribe((response: Response)=>{
    // this.recipeService.setRecipe(response)
    //
    //   })

    }


  store(){
    this.Router.navigate(['/spin2'])
  this.HttpClient.put('https://recipe-app-24479.firebaseio.com/recipes.json?auth='+ this.AuthService.getToken(),this.recipeService.getRecipe()).subscribe((response:Response)=>{

    // swal({
    //   title: "Saved!",
    //   icon: "success",


    swal.fire(
      'Saved!',
      '',
      'success'
    )



this.Router.navigate(['/recipes'])

    console.log(response);




  })

}


}
