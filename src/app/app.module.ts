import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HeaderComponent} from './header/header.component';
import { AppComponent } from './app.component';
import { Receipe } from './recipe/recipe.component';
import { rl } from './recipe/rl/rl.component';
import { RdComponent } from './recipe/rd/rd.component';
import { RiComponent } from './recipe/rl/ri/ri.component';
import {HttpClientModule} from '@angular/common/http';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { AppRoutingModule } from './app-routing.module';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { shoppingService } from './shopping-list/shopping.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { RecipeComponent } from './recipe/recipe/recipe.component';
import { RecipeEditComponent } from './recipe/recipe-edit/recipe-edit.component';
import { RecipeStartComponent } from './recipe/recipe-start/recipe-start.component';
import { FooterComponent } from './footer/footer.component';
import { recipeService } from './recipe/recipe.service';
import { DataStorageService } from './shared/storeData.service';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthService } from './auth/Auth.service';
import { SigninComponent } from './auth/signin/signin.component';
import { SpinnerComponent } from './spinner/spinner.component';

import { SpinneriiComponent } from './spinnerii/spinnerii.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { Gaurd } from './auth/Gaurd.service';






@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    Receipe,

rl,
RdComponent,
RiComponent,
ShoppingListComponent,
ShoppingEditComponent,
AboutComponent,
HomeComponent,

RecipeEditComponent,
RecipeStartComponent,
FooterComponent,
SignupComponent,
SigninComponent,
SpinnerComponent,

SpinneriiComponent,

SideNavComponent,






  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
        BrowserAnimationsModule

  ],
  providers: [shoppingService,recipeService, DataStorageService, AuthService, Gaurd],
  bootstrap: [AppComponent]
})
export class AppModule { }
