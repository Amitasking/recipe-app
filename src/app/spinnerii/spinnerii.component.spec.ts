import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinneriiComponent } from './spinnerii.component';

describe('SpinneriiComponent', () => {
  let component: SpinneriiComponent;
  let fixture: ComponentFixture<SpinneriiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpinneriiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinneriiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
