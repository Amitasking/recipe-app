import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataStorageService } from '../shared/storeData.service';
import { AuthService } from '../auth/Auth.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector:'app-header',
    templateUrl : './header.component.html'


})
export class HeaderComponent implements OnInit{

constructor(private HttpClient:HttpClient, private ActivatedRoute: ActivatedRoute, private route:Router, private DataStorageService:DataStorageService, public AuthService:AuthService){


}

ngOnInit(){
this.AuthService.e.subscribe(()=>{
  this.fetch();
})
}
store(){
  this.DataStorageService.store();

}


fetch(){

  this.DataStorageService.fetch();
}

isAuthenticate = this.AuthService.isAuthenticate();

logout(){
  this.AuthService.logout();

}


}
